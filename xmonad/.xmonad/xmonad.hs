--IMPORT
import XMonad

--VARIABLES
myTerminal    = "termite"
myModMask     = mod4Mask -- Win Key
myBorderWidth = 3

--MAIN
main = do
  xmonad $ def
    { terminal    = myTerminal
    , modMask     = myModMask
    , borderWidth = myBorderWidth
    }


